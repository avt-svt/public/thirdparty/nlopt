The folder nlopt-2.9.1 contains NLOPT version 2.9.1 as downloaded on January 22, 2025 from https://github.com/stevengj/nlopt .
For license information, please refer to the files therein. The only additions are the test folder and CMakeLists.txt in the present folder which is based on the original nlopt Cmake file.
Modifications were made in the CMakeLists.txt to allow for C++ and avoid Install packages.

This nlopt version has been tested for the following configurations:
	- Windows 10 using Microsoft Visual Studio 2017

