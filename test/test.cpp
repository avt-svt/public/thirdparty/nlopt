/**********************************************************************************
 * Copyright (c) 2019 Process Systems Engineering (AVT.SVT), RWTH Aachen University
 *
 * This file is made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 **********************************************************************************/

#include "nlopt.hpp"

#include <iostream>
#include <vector>


double
nlopt_objective(const std::vector<double>& x, std::vector<double>& grad, void* f_data)
{
    if (grad.empty()) {    // Derivative-free solver
    }
    else {    				// Gradient-based solver
		grad.resize(2);
		grad[0] = 2.*x[0]*x[1];
		grad[1] = std::pow(x[0],2) - 3.*std::pow(x[1],2.);
    }
    return std::pow(x[0],2)*x[1] - std::pow(x[1],3);
}


int
main() {

	std::cout << "Solving the problem:" << std::endl
			  << "   min y*x^2 - y^3" << std::endl
			  << "     s.t. x in [0,5]" << std::endl
			  << "          y in [0,5]" << std::endl << std::endl;

	std::vector<double> lowerBounds(2,0);
	std::vector<double> upeprBounds(2,5);

	nlopt::opt _NLopt(nlopt::LD_SLSQP, 2);
	_NLopt.set_lower_bounds(lowerBounds);
	_NLopt.set_upper_bounds(upeprBounds);
	_NLopt.set_min_objective(nlopt_objective, NULL);
	
	std::vector<double> solutionPoint(2,2.5);
	double objectiveValue;
	try{
		_NLopt.optimize(solutionPoint, objectiveValue);
	}
	catch(...) {
		// Ignoring exceptions, since NLopt throws even when reaching iteration limit
	}
	
	if (solutionPoint.size()!=2) {
		std::cout << "Error: solution point is not of size 2." << std::endl;
	} else {
		std::cout << "Solution point: " << std::endl
				  << "  x = " << solutionPoint[0] << std::endl
				  << "  y = " << solutionPoint[1] << std::endl
				  << "Objective value: " << objectiveValue << std::endl;
		std::cout << "NLOPT seems to work." << std::endl;
	}
	return 0;

}